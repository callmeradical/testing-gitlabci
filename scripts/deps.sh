#!/usr/bin/sh

KOPS_VERSION="1.9.0"
HELM_VERSION="2.9.1"

echo "$(date) - Setting up build environment..."
echo "$(date) - Installing dependencies..."
apk update > /dev/null
apk add openssh-client jq py-pip wget curl > /dev/null

echo "$(date) - Generating ssh-key..."
ssh-keygen -q -t rsa -N '' -f $HOME/.ssh/id_rsa 

echo "$(date) - Installing awscli..."
pip install --upgrade pip > /dev/null
pip install --upgrade awscli > /dev/null

echo "$(date) - Installing kops v$KOPS_VERSION"
wget -q https://github.com/kubernetes/kops/releases/download/$KOPS_VERSION/kops-linux-amd64 > /dev/null
chmod +x kops-linux-amd64 > /dev/null
mv kops-linux-amd64 /usr/local/bin/kops > /dev/null

echo "$(date) - Installing Helm v$HELM_VERSION"
wget -q https://storage.googleapis.com/kubernetes-helm/helm-v$HELM_VERSION-linux-amd64.tar.gz > /dev/null
tar -zxvf helm-v$HELM_VERSION-linux-amd64.tar.gz > /dev/null
chmod +x linux-amd64/helm > /dev/null
mv linux-amd64/helm /usr/local/bin/helm > /dev/null
rm -rf linux-amd64 > /dev/null

echo "$(date) - Finished installing dependencies."
