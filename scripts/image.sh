#!/usr/bin/env bash

args="$@"

HOST=repo-qa.naiccorp.net
REPO=builder-kops
ORG=builder-images
PORT=18443
IMAGE_NAME=$HOST:$PORT/$REPO/$ORG/kops
BRANCH=$(git branch -l | grep "*" | sed 's/* //' )
COMMIT=$(git log | grep commit | awk '{ print $2 }' | head -n1 | sed 's/commit //')
TAG=$(git describe --tags)

echo $BRANCH
echo $COMMIT
echo $TAG

DOCKER_TAGS=( latest $BRANCH $TAG $COMMIT )

tags () {
  echo "$(date) - Building build image..."
  docker build -t $IMAGE_NAME .


  for i in ${DOCKER_TAGS[@]}; do
    echo $i
    if [ "$i" != "" ]
      then
        docker tag $IMAGE_NAME $IMAGE_NAME:$i
        if [ $? == 0 ]
        then
          echo "$(date) - Tagged $IMAGE_NAME:latest as $IMAGE_NAME:$i"
        fi
        if [ "$1" == "push" ]
        then
          docker push $IMAGE_NAME:$i 
          if [ $? == 0 ]
          then
            echo "$(date) - Pushed $IMAGE_NAME:$i to remote."
          fi
        fi
    fi
  done
}


if [ $# -eq 0 ] 
then
  tags
else
  for arg in $args; do
    tags $arg
  done
fi
