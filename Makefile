.PHONY: build-image
build-image:
	scripts/image.sh

.PHONY: push-image
push-image:
	scripts/image.sh push

.PHONY: build-image-local
build-image-local:
	docker run -dti -v $(PWD):/code -v /var/run/docker.sock:/var/run/docker.sock --name shell-example alpine:latest /bin/sh
	
